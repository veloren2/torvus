FROM debian:10-slim
ARG PROJECTNAME=torvus

RUN apt-get update; export DEBIAN_FRONTEND=noninteractive; \
    apt-get install -y --no-install-recommends --assume-yes \
        ca-certificates; \
    rm -rf /var/lib/apt/lists/*;

WORKDIR /opt/app
COPY ./assets /opt/app/assets
COPY ./torvus /opt/app/torvus
ENV RUST_BACKTRACE=full
CMD ["./torvus"]
