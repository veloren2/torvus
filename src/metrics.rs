use prometheus::{IntCounter, IntGauge, Opts, Registry};
use std::{
    convert::TryInto,
    error::Error,
    time::{SystemTime, UNIX_EPOCH},
};

type RegistryFn = Box<dyn FnOnce(&Registry) -> Result<(), prometheus::Error>>;

pub struct TorvusMetrics {
    pub unix_start_time: IntGauge,

    pub veloren_connected_error: IntCounter,
    pub veloren_register_error: IntCounter,

    pub messages_from_discord: IntCounter,
    pub messages_to_discord: IntCounter,

    pub messages_from_veloren: IntCounter,
    pub messages_to_veloren: IntCounter,
}

impl TorvusMetrics {
    pub fn new() -> Result<(Self, RegistryFn), Box<dyn Error>> {
        let unix_start_time = IntGauge::with_opts(Opts::new(
            "unix_start_time",
            "start time of the server in seconds since EPOCH",
        ))?;

        let veloren_connected_error = IntCounter::with_opts(Opts::new(
            "veloren_connected_error",
            "number of reconnection attempts to the veloren server",
        ))?;
        let veloren_register_error = IntCounter::with_opts(Opts::new(
            "veloren_register_error",
            "number of register attempts to the veloren server",
        ))?;

        let messages_from_discord = IntCounter::with_opts(Opts::new(
            "messages_from_discord",
            "shows the number of message receive from discord",
        ))?;
        let messages_to_discord = IntCounter::with_opts(Opts::new(
            "messages_to_discord",
            "show the number of message send by torvus to discord",
        ))?;

        let messages_from_veloren = IntCounter::with_opts(Opts::new(
            "messages_from_veloren",
            "shows the number of message receive from veloren",
        ))?;
        let messages_to_veloren = IntCounter::with_opts(Opts::new(
            "messages_to_veloren",
            "show the number of message send by torvus to veloren",
        ))?;

        let since_the_epoch = SystemTime::now()
            .duration_since(UNIX_EPOCH)
            .expect("Time went backwards");
        unix_start_time.set(since_the_epoch.as_secs().try_into()?);
        let unix_start_time_clone = unix_start_time.clone();

        let veloren_connected_error_clone = veloren_connected_error.clone();
        let veloren_register_error_clone = veloren_register_error.clone();

        let messages_from_discord_clone = messages_from_discord.clone();
        let messages_to_discord_clone = messages_to_discord.clone();

        let messages_from_veloren_clone = messages_from_veloren.clone();
        let messages_to_veloren_clone = messages_to_veloren.clone();

        let f = |registry: &Registry| {
            registry.register(Box::new(unix_start_time_clone))?;

            registry.register(Box::new(veloren_connected_error_clone))?;
            registry.register(Box::new(veloren_register_error_clone))?;

            registry.register(Box::new(messages_from_discord_clone))?;
            registry.register(Box::new(messages_to_discord_clone))?;

            registry.register(Box::new(messages_from_veloren_clone))?;
            registry.register(Box::new(messages_to_veloren_clone))?;
            Ok(())
        };

        Ok((
            Self {
                unix_start_time,

                veloren_connected_error,
                veloren_register_error,

                messages_from_discord,
                messages_to_discord,

                messages_from_veloren,
                messages_to_veloren,
            },
            Box::new(f),
        ))
    }
}
