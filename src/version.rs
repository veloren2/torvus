use lazy_static::lazy_static;

pub const GIT_VERSION: &str = include_str!(concat!(env!("OUT_DIR"), "/githash"));
pub const GIT_TAG: &str = include_str!(concat!(env!("OUT_DIR"), "/gittag"));

lazy_static! {
    pub static ref GIT_HASH: &'static str = GIT_VERSION
        .split('/')
        .next()
        .expect("failed to retrieve git_hash!");
    pub static ref GIT_DATETIME: &'static str = GIT_VERSION
        .split('/')
        .nth(1)
        .expect("failed to retrieve git_datetime!");
    pub static ref GIT_DATE: String = GIT_DATETIME
        .split('-')
        .take(3)
        .collect::<Vec<&str>>()
        .join("-");
    pub static ref GIT_TIME: &'static str = GIT_DATETIME
        .split('-')
        .nth(3)
        .expect("failed to retrieve git_time!");
}
